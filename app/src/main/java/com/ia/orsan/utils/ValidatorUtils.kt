package com.ia.orsan.utils

import android.support.design.widget.TextInputLayout
import android.widget.EditText

class ValidatorUtils {

    companion object {

        fun isEmailValid(container: TextInputLayout, field: EditText, wrongFormat: String, emptyField: String): Boolean {
            var isValid = true

            isValid = isEmptyField(container, field, emptyField)

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(field.text.toString()).matches()) {
                isValid = false
                container.error = wrongFormat
                return isValid
            }

            container.error = null
            container.isErrorEnabled = false

            return isValid
        }

        fun isEmptyField(container: TextInputLayout, field: EditText, emptyField: String): Boolean {
            var isValid = true

            if (field.text.toString().isEmpty()) {
                isValid = false
                container.error = emptyField
                return isValid
            }

            container.error = null
            container.isErrorEnabled = false

            return isValid
        }

    }

}