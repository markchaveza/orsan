package com.ia.orsan.di.data

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ia.orsan.data.DataConfiguration
import com.ia.orsan.data.net.NetLoginEntity
import com.ia.orsan.data.net.interfaces.LoginService
import com.ia.orsan.data.repository.LoginRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DataModule(private val baseUrl: String) {

    private val logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val httpClient: OkHttpClient.Builder
    private val gson: Gson

    init {
        logging.level = HttpLoggingInterceptor.Level.BODY

        httpClient = OkHttpClient.Builder()
        httpClient.readTimeout(15, TimeUnit.SECONDS)
        httpClient.connectTimeout(15, TimeUnit.SECONDS)
        httpClient.writeTimeout(15, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging)

        gson = GsonBuilder().setLenient().create()
    }

    @Provides
    @Singleton
    fun providesDataConfiguration(): DataConfiguration = DataConfiguration(baseUrl)

    @Provides
    @Singleton
    fun providesRetrofit(dataConfiguration: DataConfiguration): Retrofit =
            Retrofit.Builder()
                    .baseUrl(dataConfiguration.getBaseUrl())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build()

    @Provides
    @Singleton
    fun providesLoginRepository(retrofit: Retrofit): LoginRepository =
            NetLoginEntity(retrofit.create(LoginService::class.java))


}