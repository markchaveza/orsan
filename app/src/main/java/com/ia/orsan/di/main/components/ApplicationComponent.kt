package com.ia.orsan.di.main.components

import com.ia.orsan.di.data.DataModule
import com.ia.orsan.di.domain.DomainModule
import com.ia.orsan.di.main.modules.ApplicationModule
import com.ia.orsan.ui.SplashActivity
import com.ia.orsan.ui.login.LoginActivity
import com.ia.orsan.ui.register.RegisterActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (DataModule::class), (DomainModule::class)])
interface ApplicationComponent {

    fun inject(splashActivity: SplashActivity)
    fun inject(loginActivity: LoginActivity)
    fun inject(registerActivity: RegisterActivity)
}