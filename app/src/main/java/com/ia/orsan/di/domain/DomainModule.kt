package com.ia.orsan.di.domain

import com.ia.orsan.data.repository.LoginRepository
import com.ia.orsan.domain.LoginInteractor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    @Singleton
    fun provideLoginInteractor(repository: LoginRepository) =
            LoginInteractor(repository)

}