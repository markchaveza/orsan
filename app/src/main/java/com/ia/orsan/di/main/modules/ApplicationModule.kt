package com.ia.orsan.di.main.modules

import android.app.Application
import android.content.Context
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.orsan.OrsanApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: OrsanApplication) {

    @Provides
    @Singleton
    fun providesApplication(): Application = application

    @Provides
    @Singleton
    fun providesContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesPreferencesHelper(context: Context): SharedPreferencesManager = SharedPreferencesManager(context)


}