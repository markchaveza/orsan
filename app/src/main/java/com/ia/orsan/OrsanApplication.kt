package com.ia.orsan

import android.app.Application
import com.facebook.stetho.Stetho
import com.ia.orsan.di.data.DataModule
import com.ia.orsan.di.main.components.ApplicationComponent
import com.ia.orsan.di.main.components.DaggerApplicationComponent
import com.ia.orsan.di.main.modules.ApplicationModule

class OrsanApplication : Application() {

    companion object {
        private lateinit var applicationComponent: ApplicationComponent

        private fun initDependencies(application: OrsanApplication) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(ApplicationModule(application))
                    .dataModule(DataModule(application.getString(R.string.base_url)))
                    .build()
        }

        fun getApplicationComponent() = applicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDependencies(this)
        Stetho.initializeWithDefaults(this)
    }


}