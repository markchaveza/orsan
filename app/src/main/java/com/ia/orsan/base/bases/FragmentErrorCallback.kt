package com.ia.orsan.base.bases

interface FragmentErrorCallback {
    fun onShowError(fragmentPosition: Int, errorViewHeight: Int)
    fun onDismissError(fragmentPosition: Int)
}