package com.ia.orsan.base

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.ia.orsan.R

/**
 * Created by rsandoval on 09/01/2018.
 */
fun ViewGroup.inflate(viewId: Int, attachRoot: Boolean = false): View? {
    return LayoutInflater.from(context).inflate(viewId, this, attachRoot)
}

fun View.snack(message: String, length: Int = Snackbar.LENGTH_INDEFINITE) {
    val snack = Snackbar.make(this, message, length)
    snack.setAction(android.R.string.ok, null)
    snack.show()
}

fun View.snackWhitAction(message: String) {
    val snack = Snackbar.make(this, message, Snackbar.LENGTH_INDEFINITE)
    snack.setAction(android.R.string.ok) {
        snack.dismiss()
    }
    snack.show()
}

fun ImageView.loadUrl(url: String, placeholder: Int = 0, error: Int = 0) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions.centerCropTransform()
                    .placeholder(placeholder)
                    .error(error))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
}

fun ImageView.loadUrlFitCenter(url: String, placeholder: Int = 0, error: Int = 0) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions.fitCenterTransform()
                    .placeholder(placeholder)
                    .error(error))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
}

fun FragmentManager.removeFragment(tag: String, slideIn: Int = R.anim.left_in, slideOut: Int = R.anim.rigth_out) {
    this.beginTransaction()
            .disallowAddToBackStack()
            .setCustomAnimations(slideIn, slideOut)
            .remove(this.findFragmentByTag(tag))
            .commit()
}

fun FragmentManager.addFragment(containerViewId: Int, fragment: Fragment, tag: String, slideIn: Int = R.anim.left_in, slideOut: Int = R.anim.rigth_out) {
    this.beginTransaction().disallowAddToBackStack()
            .setCustomAnimations(slideIn, slideOut)
            .add(containerViewId, fragment, tag)
            .commit()
}

fun FragmentManager.replaceFragment(containerViewId: Int, fragment: Fragment) {
    this.beginTransaction()
            .replace(containerViewId, fragment)
            .commit()
}

fun Drawable.overrideColor(backgroundColor: Int) {
    when (this) {
        is GradientDrawable -> setColor(backgroundColor)
        is ShapeDrawable -> paint.color = backgroundColor
        is ColorDrawable -> color = backgroundColor
    }
}