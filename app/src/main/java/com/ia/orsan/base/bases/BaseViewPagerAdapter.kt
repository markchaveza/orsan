package com.ia.orsan.base.bases

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class BaseViewPagerAdapter(fm: FragmentManager?, private val fragments: List<Fragment>, private val titles: List<String> = emptyList()) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment = this.fragments[position]

    override fun getCount(): Int = this.fragments.size

    override fun getPageTitle(position: Int): CharSequence? {
        return if (this.titles.isEmpty() || this.fragments.size != this.titles.size) {
            ""
        } else {
            this.titles[position]
        }
    }
}