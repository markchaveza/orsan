package com.ia.orsan.base.bases

import com.ia.orsan.ui.views.BaseView

open class BasePresenter<T : BaseView> {

    private var view: T? = null

    fun setView(view: T?) {
        this.view = view
    }

    fun getView(): T? = this.view

    fun initialize() {

    }

    fun terminate() {

    }


}