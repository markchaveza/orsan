package com.ia.orsan.ui.login.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(

        @field:SerializedName("Mensaje")
        val message: String? = null,

        @field:SerializedName("Codigo")
        val code: Int? = null,

        @field:SerializedName("Estatus")
        val status: Int? = null,

        @field:SerializedName("access_token")
        val accessToken: String? = null,

        @field:SerializedName("refresh_token")
        val refreshToken: String? = null,

        @field:SerializedName("token_type")
        val tokenType: String? = null,

        @field:SerializedName("expire_in")
        val expireIn: Int? = null
)