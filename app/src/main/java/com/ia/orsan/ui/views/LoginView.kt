package com.ia.orsan.ui.views

import com.ia.orsan.ui.login.model.LoginResponse

interface LoginView : BaseView {

    fun onSuccessLogin(loginResponse: LoginResponse)

    fun onFailedLogin(error: Throwable)

}