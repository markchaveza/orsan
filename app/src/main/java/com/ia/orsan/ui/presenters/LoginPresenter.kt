package com.ia.orsan.ui.presenters

import com.ia.orsan.base.bases.BasePresenter
import com.ia.orsan.data.DataConfiguration
import com.ia.orsan.domain.LoginInteractor
import com.ia.orsan.ui.login.model.LoginRequest
import com.ia.orsan.ui.login.model.LoginResponse
import com.ia.orsan.ui.views.LoginView
import javax.inject.Inject

class LoginPresenter @Inject
constructor(private val loginInteractor: LoginInteractor) : BasePresenter<LoginView>(), LoginInteractor.LoginCallback {

    init {
        this.loginInteractor.setListener(this)
    }

    /**
     * With this we invoke the api call
     * to log into our account
     * @param user to be logged in
     * @param password that belongs to the user
     */
    fun login(user: String, password: String) {
        loginInteractor.login(buildLoginRequest(user, password))
    }

    /**
     * When login succeeded
     * @param loginResponse
     */
    override fun onSuccessLogin(loginResponse: LoginResponse) {
        getView()?.let {
            it.onSuccessLogin(loginResponse)
        }
    }

    /**
     * When something went wrong with login
     * @param error
     */
    override fun onFailedLogin(error: Throwable) {
        getView()?.let {
            it.onFailedLogin(error)
        }
    }

    /**
     * Let's build LoginRequest object to invoke service
     * @param user
     * @param password
     */
    private fun buildLoginRequest(user: String, password: String): LoginRequest =
            LoginRequest(
                    DataConfiguration.CALL_ID,
                    DataConfiguration.CALL_ID_KEY,
                    user,
                    password
            )

}