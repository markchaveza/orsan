package com.ia.orsan.ui.views

interface BaseView {

    fun showLoading()

    fun hideLoading()

}