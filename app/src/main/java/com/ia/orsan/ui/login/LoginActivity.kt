package com.ia.orsan.ui.login

import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.orsan.OrsanApplication
import com.ia.orsan.R
import com.ia.orsan.base.bases.BaseActivity
import com.ia.orsan.ui.login.model.LoginResponse
import com.ia.orsan.ui.presenters.LoginPresenter
import com.ia.orsan.ui.views.LoginView
import com.ia.orsan.utils.ValidatorUtils
import com.pawegio.kandroid.textWatcher
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.content_login.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.toast
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginView {

    @Inject
    lateinit var loginPresenter: LoginPresenter

    @Inject
    lateinit var preferences: SharedPreferencesManager

    override fun getLayoutResId(): Int =
            R.layout.activity_login

    override fun initView() {
        super.initView()
        prepareActivity()
        prepareSupportActionBar()
        addTextWatchers()
        addLoginBtnListener()
    }

    private fun prepareSupportActionBar() {
        this.toolbar.title = ""
        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.toolbar.navigationIcon = getDrawable(R.drawable.ic_arrow_back_white_24dp)
        this.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun activeLoginButton() {
        this.login_btn.isEnabled = !this.login_email.text.toString().isEmpty() &&
                !this.login_password.text.toString().isEmpty()
    }

    private fun addTextWatchers() {
        this.login_email.textWatcher { onTextChanged { text, start, before, count -> activeLoginButton() } }
        this.login_password.textWatcher { onTextChanged { text, start, before, count -> activeLoginButton() } }
    }

    override fun onSuccessLogin(loginResponse: LoginResponse) {
        toast("Login exitoso")
    }

    override fun onFailedLogin(error: Throwable) {
        snackbar(login_container, error.message.toString())
    }

    override fun showLoading() {
        return Unit
    }

    override fun hideLoading() {
        return Unit
    }

    private fun initializeDagger() {
        OrsanApplication.getApplicationComponent().inject(this)
    }

    private fun prepareActivity() {
        initializeDagger()
        loginPresenter.setView(this)
    }

    private fun addLoginBtnListener() {
        this.login_btn.setOnClickListener {
            if (validateLoginForm()) {
                loginPresenter.login(login_email.text.toString(), login_password.text.toString())
            }
        }
    }


    private fun validateLoginForm(): Boolean =
            ValidatorUtils.isEmailValid(login_email_container, login_email, getString(R.string.login_error_wrong_format_email), getString(R.string.login_error_empty_user)) &&
                    ValidatorUtils.isEmptyField(login_password_container, login_password, getString(R.string.login_error_empty_password))
}