package com.ia.orsan.ui.home

import com.ia.orsan.base.bases.BaseActivity
import com.ia.orsan.R

class HomeActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_home

}
