package com.ia.orsan.ui.welcome

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ia.orsan.R
import kotlinx.android.synthetic.main.fragment_welcome_info.*

class WelcomeInfoFragment : Fragment() {

    private val title: String
        get() = this.arguments?.getString(ARG_SECTION_TITLE, "") ?: ""

    private val info: String
        get() = this.arguments?.getString(ARG_SECTION_INFO, "") ?: ""

    companion object {
        private const val ARG_SECTION_TITLE = "sectionTitle"
        private const val ARG_SECTION_INFO = "sectionInfo"

        fun newInstance(title: String, info: String): WelcomeInfoFragment {
            val welcomeInfoFragment = WelcomeInfoFragment()
            val bundle = Bundle()
            bundle.putString(ARG_SECTION_TITLE, title)
            bundle.putString(ARG_SECTION_INFO, info)
            welcomeInfoFragment.arguments = bundle
            return welcomeInfoFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_welcome_info, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.tv_title.text = this.title
        this.tv_info.text = this.info

    }

}
