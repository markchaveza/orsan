package com.ia.orsan.ui

import android.content.Intent
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.orsan.base.bases.BaseActivity
import com.ia.orsan.data.ArgumentConstants
import com.ia.orsan.ui.home.HomeActivity
import com.ia.orsan.ui.welcome.WelcomeActivity

class SplashActivity : BaseActivity() {

    private val preferences: SharedPreferencesManager by lazy { SharedPreferencesManager(this) }

    override fun getLayoutResId(): Int = 0

    override fun initView() {
        super.initView()
        manageIntent()
    }

    private fun manageIntent() {
        if (preferences.getBooleanPreference(ArgumentConstants.LOGGED, false)) {
            startActivity(Intent(this, HomeActivity::class.java))
        } else {
            startActivity(Intent(this, WelcomeActivity::class.java))
            finish()
        }
    }
}
