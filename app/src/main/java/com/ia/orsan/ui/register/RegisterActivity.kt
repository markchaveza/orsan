package com.ia.orsan.ui.register

import android.app.ProgressDialog
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.orsan.R
import com.ia.orsan.base.bases.BaseActivity
import com.pawegio.kandroid.textWatcher
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.content_register.*

class RegisterActivity : BaseActivity() {

    companion object {
        const val PERMISSIONS_REQUEST_LOCATION = 99
        const val LOCATION_ACCURACY = 50
    }

    private lateinit var dialog: ProgressDialog
    private val preferences: SharedPreferencesManager by lazy { SharedPreferencesManager(this) }
    //val locationManager:LocationManager by lazy { LocationManager(this,this,getString(R.string.getting_location)) }

    override fun getLayoutResId(): Int = R.layout.activity_register

    override fun initView() {
        super.initView()

        onPrepareSupportActionBar()
        addFieldWatchers()
    }

    private fun onPrepareSupportActionBar() {
        this.toolbar.title = ""
        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.toolbar.navigationIcon = getDrawable(R.drawable.ic_arrow_back_white_24dp)
        this.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun addFieldWatchers() {
        this.register_name.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
        this.register_email.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
        this.register_password.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
        this.register_postal_code.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
    }

    private fun activeRegisterButton() {
        this.register_btn.isEnabled = !this.register_name.text.toString().isEmpty() &&
                !this.register_email.text.toString().isEmpty() &&
                !this.register_password.text.toString().isEmpty() &&
                !this.register_postal_code.text.isEmpty()
    }

}
