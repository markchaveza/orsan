package com.ia.orsan.domain

import com.ia.orsan.data.repository.LoginRepository
import com.ia.orsan.exception.OrsanException
import com.ia.orsan.exception.OrsanNetworkException
import com.ia.orsan.ui.login.model.LoginRequest
import com.ia.orsan.ui.login.model.LoginResponse
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.IOException

class LoginInteractor(private val repository: LoginRepository) {

    private var mListener: LoginCallback? = null
    private var subscription: Subscription? = null

    fun setListener(listener: LoginCallback) {
        mListener = listener
    }

    fun login(loginRequest: LoginRequest) {
        subscription = repository.login(loginRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ loginResponse ->
                    mListener?.let {
                        it.onSuccessLogin(loginResponse)
                    }
                }, { error ->
                    mListener?.let {
                        when (error) {
                            is IOException -> it.onFailedLogin(OrsanNetworkException("Network error", error))
                            is OrsanException -> it.onFailedLogin(error as OrsanException)
                            else -> it.onFailedLogin(OrsanException("Unknown exception", error))
                        }
                    }
                })
    }

    fun stop() {
        subscription?.let {
            it.unsubscribe()
        }
    }

    interface LoginCallback {
        fun onSuccessLogin(loginResponse: LoginResponse)
        fun onFailedLogin(error: Throwable)
    }

}