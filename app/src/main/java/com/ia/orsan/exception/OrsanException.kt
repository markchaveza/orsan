package com.ia.orsan.exception

open class OrsanException : Exception {

    /**
     * Generates Orsan exception
     * @param detailMessage message
     */
    constructor(detailMessage: String) : super(detailMessage) {}

    /**
     * Generates Orsan exception
     * @param detailMessage message
     * @param throwable original throwable
     */
    constructor(detailMessage: String, throwable: Throwable) : super(detailMessage, throwable) {}

}