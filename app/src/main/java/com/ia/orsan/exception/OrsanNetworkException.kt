package com.ia.orsan.exception

class OrsanNetworkException : OrsanException {

    constructor(detailedMessage: String) : super(detailedMessage) {}

    constructor(detailedMessage: String, throwable: Throwable) : super(detailedMessage, throwable) {}

}