package com.ia.orsan.data

class ArgumentConstants {

    companion object {
        const val PREFS_FILENAME = "orsan.preferences"
        const val LOGGED = "logged"
        const val TUTORIAL_WAS_SHOWN = "tutorialWasShown"
        const val USER_NAME = "userName"
        const val USER_EMAIL = "userEmail"
        const val USER_AVATAR_URL = "userAvatarUrl"
        const val IS_FACEBOOK_LOGIN = "facebookLogin"
        const val TOKEN = "token"
        const val POSTAL_CODE = "postalCode"
        const val LOCATION_ALLOWED = "locationAllowed"
        const val IS_FIRST_TIME = "isFirstTime"
    }

}