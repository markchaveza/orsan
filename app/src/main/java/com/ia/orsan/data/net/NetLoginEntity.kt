package com.ia.orsan.data.net

import com.ia.orsan.data.net.interfaces.LoginService
import com.ia.orsan.data.net.utils.ResponseUtils
import com.ia.orsan.data.repository.LoginRepository
import com.ia.orsan.ui.login.model.LoginRequest
import com.ia.orsan.ui.login.model.LoginResponse
import rx.Observable

class NetLoginEntity(private val loginService: LoginService) : LoginRepository {

    override fun login(loginRequest: LoginRequest): Observable<LoginResponse> =
            loginService.login(loginRequest.callId, loginRequest.callIdKey, loginRequest.user, loginRequest.password)
                    .flatMap {
                        if (it.isSuccessful) {
                            if (it.body().status == null) {
                                Observable.just(it.body())
                            } else {
                                Observable.error(ResponseUtils.processWrongBody(it))
                            }
                        } else {
                            Observable.error(ResponseUtils.processErrorResponse(it))
                        }
                    }

}