package com.ia.orsan.data.net.interfaces

import com.ia.orsan.data.DataConfiguration
import com.ia.orsan.ui.login.model.LoginResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST
import rx.Observable

interface LoginService {

    @Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")
    @POST(DataConfiguration.LOGIN)
    @FormUrlEncoded
    fun login(
            @Field("CallId") callId: String,
            @Field("CallIdKey") callIdKey: String,
            @Field("usuario") user: String,
            @Field("pass") pass: String
    ): Observable<Response<LoginResponse>>


}