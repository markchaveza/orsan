package com.ia.orsan.data.entities

import rx.Observable

interface LoginEntity {

    fun login(user: String, password: String): Observable<String>

}