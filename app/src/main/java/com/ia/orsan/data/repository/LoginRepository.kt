package com.ia.orsan.data.repository

import com.ia.orsan.ui.login.model.LoginRequest
import com.ia.orsan.ui.login.model.LoginResponse
import rx.Observable

interface LoginRepository {

    fun login(loginRequest: LoginRequest): Observable<LoginResponse>

}