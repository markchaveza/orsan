package com.ia.orsan.data.net.utils

import android.text.TextUtils
import com.google.gson.Gson
import com.ia.orsan.exception.OrsanException
import com.ia.orsan.exception.OrsanHttpException
import com.ia.orsan.exception.OrsanNetworkException
import com.ia.orsan.exception.OrsanUnknownException
import com.ia.orsan.ui.login.model.LoginResponse
import retrofit2.Response
import java.io.IOException

class ResponseUtils {

    private constructor() {}

    companion object {
        /**
         * Obtains a failure response and transform it into a CinepolisException
         *
         * @param response Error response from retrofit
         * @return Custom Cinepolis Exception
         */
        fun processErrorResponse(response: Response<*>): OrsanException {
            try {
                val errorBody = response.errorBody().string()
                return if (TextUtils.isEmpty(errorBody)) {
                    if (response.code() == 504) {
                        OrsanNetworkException("Network error")
                    } else {
                        OrsanUnknownException(response.message(), response.code())
                    }
                } else {
                    val gson = Gson()
                    val error = gson.fromJson(errorBody, Error::class.java)
                    val codeError = response.code()
                    OrsanHttpException(error.message.toString(), codeError)
                }
            } catch (ioException: IOException) {
                return OrsanUnknownException("Error parsing message", ioException, response.code())
            }
        }

        /**
         * Obtains a failure response out of a
         * 200 OK when it exists
         * @param response
         */
        fun processWrongBody(response: Response<*>): OrsanException {
            return try {
                val body = response.body()
                OrsanException((body as LoginResponse).message.toString())
            } catch (ioException: IOException) {
                OrsanUnknownException("Error parsing message", ioException, response.code())
            }
        }
    }

}